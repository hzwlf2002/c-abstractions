#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "sort.h"

#define SIZE 10000000

int
main() {
	/* large array cannot get memory from stack, but from heap */
	int *a = malloc(sizeof(int) * SIZE);
	int i;
	srand(time(NULL));
	for (i = 0; i < SIZE; i++) {
		a[i] = rand();
	}

	/*
	for (i = 0; i < SIZE; i++) {
		printf("%d:%d\n", i, a[i]);
	}
	*/

	//QuickSort(a, SIZE);
	//SelectSort(a, SIZE);
	MergeSort(a, SIZE);
	printf("----------------------------\n");
	printf("after sort\n");

	/*
	for (i = 0; i < SIZE; i++) {
		printf("%d:%d\n", i, a[i]);
	}
	*/

	return EXIT_SUCCESS;
}
