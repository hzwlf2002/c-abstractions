#include "sort.h"

void SelectSort(int array[], int n) {
	int lh, rh, i, temp;
	for (lh = 0; lh < n; lh++) {
		rh = lh;
		for (i = lh + 1; i < n; i++) {
			if (array[i] < array[rh]) {
				rh = i;
			}
		}

		temp = array[lh];
		array[lh] = array[rh];
		array[rh] = temp;
	}
}
