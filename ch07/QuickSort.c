#include "genlib.h"
static int Partition(int array[], int n);

void
QuickSort(int array[], int n) {
	int boundary;
	if (n < 2) return;

	boundary = Partition(array, n);
	QuickSort(array, boundary);
	QuickSort(array + boundary + 1, n - boundary - 1);
}

static int
Partition(int array[], int n) {
	int lh, rh, pivot, temp;
	pivot = array[0];
	lh = 1;
	rh = n - 1;
	while (TRUE) {
		while (lh < rh && array[rh] >= pivot) rh--;
		while (lh < rh && array[lh] < pivot) lh++;
		if (lh == rh) break;
		temp = array[lh];
		array[lh] = array[rh];
		array[rh] = temp;
	}

	if (array[lh] >= pivot) return (0);
	array[0] = array[lh];
	array[lh] = pivot;
	return (lh);
}
