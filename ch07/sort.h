#ifndef sort_h
#define sort_h
void SelectSort(int array[], int n);
void MergeSort(int array[], int n);
void QuickSort(int array[], int n);

#endif
