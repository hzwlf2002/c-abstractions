#include "genlib.h"

static int * CopySubArray(int array[], int start, int n);
static void Merge(int array[], int arr1[], int n1, 
		int arr2[], int n2) ;
void 
MergeSort(int array[], int n) {
	int n1, n2, *arr1, *arr2;

	if (n <= 1) return;
	n1 = n / 2;
	n2 = n - n1;

	arr1 = CopySubArray(array, 0, n1);
	arr2 = CopySubArray(array, n1, n2);
	MergeSort(arr1, n1);
	MergeSort(arr2, n2);

	Merge(array, arr1, n1, arr2, n2);
	FreeBlock(arr1);
	FreeBlock(arr2);
}

static void
Merge(int array[], int arr1[], int n1, 
		int arr2[], int n2) {
	int p, p1, p2;
	p = p1 = p2 = 0;
	while (p1 < n1 && p2 < n2) {
		if (arr1[p1] < arr2[p2]) {
			array[p++] = arr1[p1++];
		} else {
			array[p++] = arr2[p2++];
		}
	}

	/* only execute one while */
	while (p1 < n1) array[p++] = arr1[p1++];
	while (p2 < n2) array[p++] = arr2[p2++];
}

static int *
CopySubArray(int array[], int start, int n) {
	int i, *result;
	result = NewArray(n, int);
	for (i = 0; i < n; i++) {
		result[i] = array[start + i];	
	}
	return(result);
}
