#include <stdio.h>
#include <ctype.h>
#include "genlib.h"
#include "buffer.h"
#include "simpio.h"

static void ExecuteCommand(bufferADT buffer, string line);
static void HelpCommand(void);

int
main() {
	bufferADT buffer;
	buffer = NewBuffer();
	while (TRUE) {
		printf("* ");
		ExecuteCommand(buffer, GetLine());
		DisplayBuffer(buffer);
	}
	FreeBuffer(buffer);

	return EXIT_SUCCESS;
}

static void ExecuteCommand(bufferADT buffer, string line) {
	int i;
	switch (toupper(line[0])) {
		case 'I': 
			for (i = 1; line[i] != '\0'; i++) {
				InsertCharacter(buffer, line[i]);
			}
			break;
		case 'D':
			DeleteCharacter(buffer);
			break;
		case 'F':
			MoveCursorForward(buffer);
			break;
		case 'B':
			MoveCursorBackward(buffer);
			break;
		case 'J':
			MoveCursorToStart(buffer);
			break;
		case 'E':
			MoveCursorToEnd(buffer);
			break;
		case 'H':
			HelpCommand();
			break;
		case 'Q':
			exit(EXIT_FAILURE);
		default: 
			printf("illegal command\n");
			break;
	}
}

static void
HelpCommand(void) {
	printf("Use the following commands to edit the buffer: \n");
	printf("%-8sInsert text up to the end of the line\n", "I...");
	printf("%-8sMove forward a character\n", "F");
	printf("%-8sMove backward a character\n", "B");
	printf("%-8sJumps to the beginning of the buffer\n", "J");
	printf("%-8sJumps to the end of the buffer\n", "E");
	printf("%-8sDeletes the next character\n", "D");
	printf("%-8sGenerates a help message\n", "H");
	printf("%-8sQuits the program\n", "Q");
}
