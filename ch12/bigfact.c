#include <stdio.h>
#include "genlib.h"
#include "bigint.h"

#define LowerLimit 0
#define UpperLimit 100

static bigIntADT BigFact(int n);

int main(void)
{
	int i;
	
	for (i = LowerLimit; i <= UpperLimit; i++) {
		printf("%2d! = %s\n", i, BigIntToString(BigFact(i)));	
	}

	bigIntADT n = AddBigInt(NewBigInt(111111111), NewBigInt(222222222));
	bigIntADT m = MultiplyBigInt(NewBigInt(111111111), NewBigInt(222222222));
	printf("%s\n", BigIntToString(n));
	printf("%s\n", BigIntToString(m));
	
	return 0;
}

static bigIntADT BigFact(int n) {
	if (n == 0) {
		return (NewBigInt(1));
	} else {
		return (MultiplyBigInt(NewBigInt(n), BigFact(n - 1)));
	}
}
