/*
 * File: list.h
 * ------------
 * This interface defines an abstraction for an immutable list.
 */
#ifndef list_h
#define list_h
#include "genlib.h"

typedef void *listElementT;

typedef struct listCDT *listADT;

listADT NewList(void);

listADT ListCons(listElementT head, listADT tail);

listElementT ListHead(listADT list);

listADT ListTail(listADT list);

int ListLength(listADT list);

listElementT NthElement(listADT list, int n);

static void RecDisplayList(listADT list);
	
void DisplayStringList(listADT list);


#endif
