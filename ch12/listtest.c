#include <stdio.h>
#include <stdlib.h>
#include "genlib.h"
#include "list.h"

int 
main()
{
	listADT l = malloc(sizeof(l));

	char *str1 = "hello";
	l = ListCons(str1, l);

	char *str2 = "world";
	l = ListCons(str2, l);

	printf("%s\n", NthElement(l, 0));

	int len = ListLength(l);
	printf("%d\n", len);
	
	return EXIT_SUCCESS;
}
