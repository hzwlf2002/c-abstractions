/*
 * File: list.c
 * ------------
 * This file implements the list.h interface, which defines
 * an immutable abstraction for manipulating lists of values.
 */

#include <stdio.h>
#include "genlib.h"
#include "list.h"

struct listCDT {
	listElementT head;
	listADT tail;
};

listADT ListCons(listElementT head, listADT tail) {
	listADT list;

	list = New(listADT);
	list -> head = head;
	list -> tail = tail;
	return (list);
}

listElementT ListHead(listADT list) {
	if (list == NULL) Error("ListHead called on NULL list");
	return (list -> head);
}

listADT ListTail(listADT list) {
	if (list == NULL) Error("ListTail called on NULL list");
	return (list -> tail);
}

int ListLength(listADT list) {
	if (list == NULL) {
		return 0;
	} else {
		return (ListLength(ListTail(list)) + 1);
	}
}

listElementT NthElement(listADT list, int n) {
	if (list == NULL) {
		Error("NthElement: No such element");
	} else if (n == 0) {
		return (ListHead(list));
	} else {
		return (NthElement(ListTail(list), n - 1));
	}
}

void DisplayStringList(listADT list) {
	printf("(");
	RecDisplayList(list);
	printf(")\n");
}

static void RecDisplayList(listADT list) {
	if (list != NULL) {
		printf("%s", ListHead(list));
	}
	if (ListTail(list) != NULL) printf(" ");
	RecDisplayList(ListTail(list));
}


