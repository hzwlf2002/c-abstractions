#include <stdio.h>
#include <ctype.h>

#include "genlib.h"
#include "strlib.h"
#include "bigint.h"

struct bigIntCDT {
	int finalDigit;
	bigIntADT leadingDigits;
};

static bigIntADT AddWithCarry(bigIntADT n1, bigIntADT n2, int carry);
static bigIntADT MultiplyDigit(int digit, bigIntADT n);
static bigIntADT DigitCons(bigIntADT n, int digit);
static bigIntADT LeadingDigits(bigIntADT n);
static int FinalDigit(bigIntADT n);

bigIntADT NewBigInt(int i) {
	if (i < 0) Error("Negative integers are not permited");
	if (i == 0) return (NULL);
	return (DigitCons(NewBigInt(i / 10), i % 10));
}

bigIntADT StringToBigInt(string str) {
	int len;
	char ch;

	len = StringLength(str);
	if (len == 0) return NULL;
	ch = str[len - 1];
	if (!isdigit(ch)) Error("illegal digit %c", ch);
	return (DigitCons(StringToBigInt(SubString(str, 0, len - 2)),
				ch - '0'));
}

string BigIntToString(bigIntADT n) {
	string str;

	str = CharToString(FinalDigit(n) + '0');
	if (LeadingDigits(n) != NULL) {
		str = Concat(BigIntToString(LeadingDigits(n)), str);
	}
	return (str);
}

bigIntADT AddBigInt(bigIntADT n1, bigIntADT n2) {
	return (AddWithCarry(n1, n2, 0));
}

static bigIntADT AddWithCarry(bigIntADT n1, bigIntADT n2, int carry) {
	int sum;
	bigIntADT p1, p2;

	p1 = LeadingDigits(n1);
	p2 = LeadingDigits(n2);
	sum = FinalDigit(n1) + FinalDigit(n2) + carry;
	if (sum == 0 && p1 == NULL && p2 == NULL) return NULL;
	return (DigitCons(AddWithCarry(p1, p2, sum / 10), sum % 10));
}

bigIntADT MultiplyBigInt(bigIntADT n1, bigIntADT n2) {
	if (n1 == NULL) return (NULL);
	return (AddBigInt(MultiplyDigit(FinalDigit(n1), n2), MultiplyBigInt(LeadingDigits(n1),
						DigitCons(n2, 0))));
}

static bigIntADT MultiplyDigit(int digit, bigIntADT n) {
	if (digit == 0) return (NULL);
	return (AddBigInt(n, MultiplyDigit(digit - 1, n)));
}

static bigIntADT DigitCons(bigIntADT leadingDigits, int finalDigit) {
	bigIntADT cp;

	if (leadingDigits == NULL && finalDigit == 0) return (NULL);
	cp = New(bigIntADT);
	cp -> finalDigit = finalDigit;
	cp -> leadingDigits = leadingDigits;
	return (cp);
}

static bigIntADT LeadingDigits(bigIntADT n) {
	return ((n == NULL) ? NULL: n -> leadingDigits);
}

static int FinalDigit(bigIntADT n) {
	return ((n == NULL) ? 0 : n -> finalDigit);
}
