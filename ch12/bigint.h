/*
 * File: bigint.h
 * --------------
 * This interface is a primitive version of a facility intended 
 * to support integers of an arbitrary size. This package has the 
 * following limitations, which significantly reduce its utility:
 * 1. The package does not support negative integers.
 * 2. The only operations are addition and multiplication.
 * 3. It is inefficient in terms of both space and time.
 */

#ifndef bigint_h
#define bigint_h

#include "genlib.h"

typedef struct bigIntCDT *bigIntADT;

bigIntADT NewBigInt(int i);

bigIntADT StringToBigInt(string str);
string BigIntToString(bigIntADT n);

bigIntADT AddBigInt(bigIntADT n1, bigIntADT n2);

bigIntADT MultiplyBigInt(bigIntADT n1, bigIntADT n2);

#endif
