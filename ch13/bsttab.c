#include <stdio.h>
#include <stdlib.h>
#include "cmpfn.h"
#include "symtab.h"
#include "bst.h"
#include "genlib.h"
#include "strlib.h"

struct symtabCDT {
	bstADT bst;
};

typedef struct {
	string key;
	void *value;
} symtabNodeT;

static void InitEntry(void *np, void *kp, void *clientData);

symtabADT NewSymbolTable(void) {
	symtabADT table;
	table = New(symtabADT);
	table -> bst = NewBST(sizeof(symtabNodeT), (cmpFnT)StringCmpFn, InitEntry);
	return table;
}

void Enter(symtabADT table, string key, void *value) {
	symtabNodeT *np;

	np = InsertBSTNode(table -> bst, &key, NULL);
	np -> value = value;
}

void *Lookup(symtabADT table, string key) {
	symtabNodeT *np;
	np = FindBSTNode(table -> bst, &key);
	if (np == NULL) return UNDEFINED;
	return (np -> value);
}

static void InitEntry(void *np, void *kp, void *clientData) {
	((symtabNodeT *)np) -> key = CopyString(*((string *) kp));
}
