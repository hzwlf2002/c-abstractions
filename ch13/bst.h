/*
 * File: bst.h
 * -----------
 * This file provides an interface for a general binary search
 * tree facility that allows the client to maintain control of
 * the structure of the node.
 */

#ifndef bst_h
#define bst_h

#include "genlib.h"
#include "cmpfn.h"

typedef struct bstCDT *bstADT;

typedef void (*nodeFnT)(void *np, void *clientData);

typedef void (*nodeInitFnT)(void *np, void *kp, void *clientData);

bstADT NewBST(int size, cmpFnT cmpFn, nodeInitFnT nodeInitFn);

void FreeBST(bstADT bst, nodeFnT freeNodeFn);

void *FindBSTNode(bstADT bst, void *kp);

void *InsertBSTNode(bstADT bst, void *kp, void *clientData);

void *DeleteBSTNode(bstADT bst, void *kp);

typedef enum {InOrder, PreOrder, PostOrder} traversalOrderT;

void MapBST(nodeFnT fn, bstADT bst, traversalOrderT order,
		void *clientData);

void *BSTRoot(bstADT bst);
void *BSTLeftChild(bstADT bst, void *np);
void *BSTRightChild(bstADT bst, void *np);

#endif
