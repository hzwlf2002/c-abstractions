#include <stdio.h>
#include <stdlib.h>

#include "genlib.h"
#include "symtab.h"

int 
main()
{
	symtabADT table = NewSymbolTable();
	Enter(table, "王李锋", "citicbank");
	Enter(table, "朱贤军", "alibaba");
	Enter(table, "叶雯", "MSD");
	int i;
#define N 300
	for (i = 0; i < N; i++) {
		char *str = malloc(sizeof(str) * 10);
		sprintf(str, "%d", i);
		char *value = malloc(sizeof(char) * 10);
		sprintf(value, "%d", 3 * i);
		Enter(table, str, value);
	}
	
	string s = Lookup(table, "王李锋");
	printf("%s:%s\n", "100", s);

	return 0;
}
