/*
 * File: bst.c
 * ----------
 * This file implements the bst.h interface, which provides a
 * general implementation of binary search trees.
 */

#include <stdio.h>
#include "genlib.h"
#include "cmpfn.h"
#include "bst.h"

typedef void* treeT;

struct bstCDT {
	treeT root;
	int userSize, totalSize;
	cmpFnT cmpFn;
	nodeInitFnT nodeInitFn;
};

typedef struct {
	treeT left, right;
} bstDataT;

static treeT *RecFindNode(bstADT bst, treeT t, void *kp);
static void *RecInsertNode(bstADT bst, treeT *tptr, void *kp,
			void *clientData);
static void *RecDeleteNode(bstADT bst, treeT *tptr, void *kp);
static void *DeleteTargetNode(bstADT bst, treeT *tptr);
static void RecMapBST(nodeFnT fn, bstADT bst, treeT t,
				traversalOrderT order, void *cientData);
static bstDataT *BSTData(bstADT bst, treeT t);

bstADT NewBST(int size, cmpFnT cmpFn, nodeInitFnT nodeInitFn) {
	bstADT bst;

	bst = New(bstADT);
	bst -> root = NULL;
	bst -> userSize = size;
	bst -> totalSize = bst -> userSize + sizeof(bstDataT);
	bst -> cmpFn = cmpFn;
	bst -> nodeInitFn = nodeInitFn;
	return (bst);
}

void FreeBST(bstADT bst, nodeFnT freeNodeFn) {
	MapBST(freeNodeFn, bst, PostOrder, NULL);
	FreeBlock(bst);
}

void *FindBSTNode(bstADT bst, void *kp) {
	return (RecFindNode(bst, bst -> root, kp));
}

static treeT *RecFindNode(bstADT bst, treeT t, void *kp) {
	bstDataT *dp;
	int sign;

	if (t == NULL) return (NULL);
	sign = bst -> cmpFn(kp, t);
	if (sign == 0) return (t);
	dp = BSTData(bst, t);
	if (sign < 0) {
		return (RecFindNode(bst, dp -> left, kp));
	} else {
		return (RecFindNode(bst, dp -> right, kp));
	}
}

void *InsertBSTNode(bstADT bst, void *kp, void *clientData) {
	return (RecInsertNode(bst, &bst -> root, kp, clientData));
}

static void *RecInsertNode(bstADT bst, treeT *tptr, void *kp,
		void *clientData) {
	bstDataT *dp;
	treeT t;
	int sign;

	t = *tptr;
	if (t == NULL) {
		t = GetBlock(bst -> totalSize);
		bst -> nodeInitFn(t, kp, clientData);
		dp = BSTData(bst, t);
		dp -> left = dp -> right = NULL;
		*tptr = t;
		return t;
	}
	sign = bst -> cmpFn(kp, t);
	if (sign == 0) return t;
	dp = BSTData(bst, t);
	if (sign < 0) {
		return (RecInsertNode(bst, &dp -> left, kp, clientData));
	} else {
		return (RecInsertNode(bst, &dp -> right, kp, clientData));
	}
}

void *DeleteBSTNode(bstADT bst, void *kp) {
	return (RecDeleteNode(bst, &bst -> root, kp));
}

static void *RecDeleteNode(bstADT bst, treeT *tptr, void *kp) {
	bstDataT *dp;
	treeT t;
	int sign;

	t = *tptr;
	if (t == NULL) return (NULL);
	sign = bst -> cmpFn(kp, t);
	if (sign == 0) {
		return (DeleteTargetNode(bst, tptr));
	}
	dp = BSTData(bst, t);
	if (sign < 0) {
		return (RecDeleteNode(bst, &dp -> left, kp));
	} else {
		return (RecDeleteNode(bst, &dp -> right, kp));
	}
}

static void *DeleteTargetNode(bstADT bst, treeT *tptr) {
	treeT target, *rptr;
	bstDataT *tdp, *rdp;
	target = *tptr;
	tdp = BSTData(bst, target);

	if (tdp -> left == NULL) {
		*tptr = tdp -> right;
	} else if (tdp -> right == NULL) {
		*tptr = tdp -> left;	
	} else {
		rptr = &tdp -> left;
		rdp = BSTData(bst, *rptr);
		while (rdp -> right != NULL) {
			rptr = &rdp -> right;
			rdp = BSTData(bst, *rptr);
		}
		*tptr = *rptr;
		*rptr = rdp -> left;
		rdp -> left = tdp -> left;
		rdp -> right = tdp -> right;
	}
	return target;
}

void MapBST(nodeFnT fn, bstADT bst, traversalOrderT order,
			void *clientData) {
	RecMapBST(fn, bst, bst -> root, order, clientData);
}

static void RecMapBST(nodeFnT fn, bstADT bst, treeT t,
			traversalOrderT order, void *clientData) {
	bstDataT *dp;
	if (t != NULL) {
		dp = BSTData(bst, t);
		if (order == PreOrder) fn(t, clientData);
		RecMapBST(fn, bst, dp -> left, order, clientData);
		if (order == InOrder) fn(t, clientData);
		RecMapBST(fn, bst, dp -> right, order, clientData);
		if (order == PostOrder) fn(t, clientData);
	}
}

void *BSTRoot(bstADT bst) {
	return (bst -> root);
}

void *BSTLeftChild(bstADT bst, void *np) {
	bstDataT *dp;
	if (np == NULL) Error("BSTLeftChild: Argument is NULL");
	dp = BSTData(bst, np);
	return (dp -> left);
}

void *BSTRightChild(bstADT bst, void *np) {
	bstDataT *dp;
	if (np == NULL) Error("BSTRightChild: Argument is NULL");
	dp = BSTData(bst, np);
	return (dp -> right);
}

static bstDataT *BSTData(bstADT bst, treeT t) {
	return (bstDataT *) ((char *) t + bst -> userSize);
}

