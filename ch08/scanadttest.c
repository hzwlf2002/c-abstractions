/* 
 * author: wanglifeng
 * filename: scanadttest.c
 * date: 2015-07-26
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "scanadt.h"
int
main() {
	
	scannerADT scanner = NewScanner();
	SetScannerString(scanner, "This is a test string");
	SetScannerSpaceOption(scanner, IgnoreSpaces);
	while (MoreTokenExist(scanner)) {
		printf("%s\n", ReadToken(scanner));
	}
	return EXIT_SUCCESS;
}

