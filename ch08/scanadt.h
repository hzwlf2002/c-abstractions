#ifndef scanadt_h
#define scanadt_h

#include "genlib.h"

typedef struct scannerCDT* scannerADT;

scannerADT NewScanner(void);

void FreeScanner(scannerADT scanner);

void SetScannerString(scannerADT scanner, string str);

string ReadToken(scannerADT scanner);

bool MoreTokenExist(scannerADT scanner);

void SaveToken(scannerADT scanner, string token);

typedef enum {PreserveSpaces, IgnoreSpaces} spaceOptionT;

void SetScannerSpaceOption(scannerADT scanner, spaceOptionT option);

spaceOptionT GetScannerSpaceOption(scannerADT scanner);

#endif
