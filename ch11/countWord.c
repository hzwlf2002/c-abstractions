#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "genlib.h"
#include "strlib.h"
#include "simpio.h"
#include "scanadt.h"
#include "symtab.h"

typedef struct {
	int count;
} *counterT;

static void RecordWord(symtabADT table, string word) ;
static void DisplayWordFrequencies(symtabADT table) ;
void DisplayEntry(string key, void *value, void *clientData) ;

int main()
{
	FILE *infile;
	string line, token, filename;
	scannerADT scanner;
	symtabADT table;

	scanner = NewScanner();
	table = NewSymbolTable();
	printf("Input file: ");
	filename = GetLine();
	infile = fopen(filename, "r");
	if (infile == NULL) Error("can't open %s", filename);
	while ( (line = ReadLine(infile)) != NULL) {
		SetScannerString(scanner, ConvertToLowerCase(line));
		while (MoreTokenExist(scanner)) {
			token = ReadToken(scanner);
			if (isalpha (token[0])) RecordWord(table, token);
		}
	}
	fclose(infile);
	DisplayWordFrequencies(table);

	return EXIT_SUCCESS;
}

static void RecordWord(symtabADT table, string word) {
	counterT entry;

	entry = Lookup(table, word);
	if (entry == UNDEFINED) {
		entry = New(counterT);
		entry -> count = 0;
		Enter(table, word, entry);
	}
	entry -> count ++;
}

static void DisplayWordFrequencies(symtabADT table) {
	printf("Word Frequency table: \n");
	MapSymbolTable(DisplayEntry, table, NULL);
}

void DisplayEntry(string key, void *value, void *clientData) {
	printf("%-15s%5d\n", key, ((counterT) value) -> count);
}

