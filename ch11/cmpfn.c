#include "string.h"
#include "cmpfn.h"


int 
IntCmpFn(const void *p1, const void *p2) {
	int v1, v2;

	v1 = * ((int *)p1);
	v2 = * ((int *)p2);
	if (v1 == v2) return 0;
	return ( (v1 < v2) ? -1 : 1);
}

int 
StringCmpFn(const void *p1, const void *p2) {
	char *v1, *v2;

	v1 = (char *)p1;
	v2 = (char *)p2;
	if (strcmp(v1, v2)) return 0;
	return ( (strcmp(v1, v2) < 0) ? -1 : 1);
}
