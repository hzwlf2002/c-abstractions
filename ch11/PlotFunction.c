/* 
 * author: wanglifeng
 * filename: PlotFunction.c
 * date: 2015-07-25
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "genlib.h"
#include "graphics.h"
#include "xdisplay.h"

#define DeltaX 0.01

typedef double(*doubleFnT) (double);
void PlotFunction(doubleFnT fn, double start, double finish);
double myfunc(double x);

int
main() {
	InitGraphics();
	PlotFunction(myfunc, 0, 2 * M_PI);
	
	return EXIT_SUCCESS;
}

void
PlotFunction(doubleFnT fn, double start, double finish) {
	double x;
	MovePen(start, fn(start));
	for (x = start + DeltaX; x <= finish; x += DeltaX) {
		DrawLine(DeltaX, fn(x+DeltaX) - fn(x));	
	}
}

double myfunc(double x) {
	return 2 * sin(x) + 2;
}
