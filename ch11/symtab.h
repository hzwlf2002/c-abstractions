/*
 * File: symtab.h
 * --------------
 * This interface exports a simple symbol table abstraction
 */

#ifndef symtab_h
#define symtab_h

#include "genlib.h"

typedef struct symtabCDT *symtabADT;

typedef void (* symtabFnT) (string key, void *value, void *clientData);

symtabADT NewSymbolTable(void);

void FreeSymbolTable(symtabADT table);

void Enter(symtabADT table, string key, void *value);

void* Lookup(symtabADT table, string key);

void MapSymbolTable(symtabFnT fn, symtabADT table, void *clientData);

#endif
