/*
 * File: symtab.c
 * --------------
 * This file implements the symbol table abstraction.
 */

#include <stdio.h>
#include "genlib.h"
#include "strlib.h"
#include "symtab.h"

#define NBuckets 101

typedef struct cellT {
	string key;
	void *value;
	struct cellT *link;
} cellT;

struct symtabCDT {
	cellT *buckets[NBuckets];
};

static void FreeBucketChain(cellT *cp);
static cellT *FindCell(cellT *cp, string s);
static int Hash(string s, int nBuckets);

symtabADT NewSymbolTable(void) {
	symtabADT table;
	int i;

	table = New(symtabADT);
	for (i = 0; i < NBuckets; i++) {
		table -> buckets[i] = NULL;	
	}

	return table;
}

void FreeSymbolTable(symtabADT table) {
	int i;

	for (i = 0; i < NBuckets; i++) {
		FreeBucketChain(table -> buckets[i]);	
	}
	FreeBlock(table);
}

void Enter(symtabADT table, string key, void *value) {
	int bucket;
	cellT *cp;

	bucket = Hash(key, NBuckets);
	cp = FindCell(table -> buckets[bucket], key);
	if (cp == NULL) {
		cp = New(cellT *);
		cp -> key = CopyString(key);
		cp -> link = table -> buckets[bucket];
		table -> buckets[bucket] = cp;
	}
	cp -> value = value;
}

void *Lookup(symtabADT table, string key) {
	int bucket;
	cellT *cp;

	bucket = Hash(key, NBuckets);
	cp = FindCell(table -> buckets[bucket], key);
	if (cp == NULL) return (UNDEFINED);
	return (cp -> value);
}

static void FreeBucketChain(cellT *cp) {
	cellT *next;

	while (cp != NULL) {
		next = cp -> link;
		FreeBlock(cp -> key);
		FreeBlock(cp);
		cp = next;
	}
}

static cellT *FindCell(cellT *cp, string key) {
	while (cp != NULL && !StringEqual(cp -> key, key)) {
		cp = cp -> link;
	}
	return cp;
}

#define Multiplier  -1664117991L

static int Hash(string s, int nBuckets) {
	int i;
	unsigned long hashcode;

	hashcode = 0;
	for (i = 0; s[i] != '\0'; i++) {
		hashcode = hashcode * Multiplier + s[i];	
	}
	return (hashcode % nBuckets);
}

void MapSymbolTable(symtabFnT fn, symtabADT table, void *clientData) {
	int i;
	cellT *cp;

	for (i = 0; i < NBuckets; i++) {
		for (cp = table -> buckets[i]; cp != NULL; cp = cp -> link) {
			fn(cp -> key, cp -> value, clientData);
		}
	}
}
