/*
 * File: cmpfn.h
 * -------------
 * This interface exports serveral comparison functions for use
 * with ANSI library functions like qsort and bsearch as well
 * as various functions in the extended library.
 */

#ifndef cmpfn_fh
#define cmpfn_fh

typedef int (*cmpFnT) (const void *p1, void *p2);

int IntCmpFn(const void *p1, const void *p2);
int ShortCmpFn(const void *p1, const void *p2);
int LongCmpFn(const void *p1, const void *p2);
int UnsignedCmpFn(const void *p1, const void *p2);
int UnsignedShortCmpFn(const void *p1, const void *p2);
int UnsignedLongCmpFn(const void *p1, const void *p2);
int CharLongCmpFn(const void *p1, const void *p2);
int FloatLongCmpFn(const void *p1, const void *p2);
int DoubleLongCmpFn(const void *p1, const void *p2);
int StringCmpFn(const void *p1, const void *p2);
int PrtCmpFn(const void *p1, const void *p2);

#endif
