#include <stdio.h>
#include <stdlib.h>

#include "genlib.h"
#include "symtab.h"

int 
main()
{
	symtabADT table = NewSymbolTable();
	Enter(table, "王李锋", "citicbank");
	Enter(table, "朱贤军", "alibaba");
	Enter(table, "叶雯", "MSD");
	int i;
	char str[7];
#define N 200
	for (i = 0; i < N; i++) {
		sprintf(str, "%d", i);
		char *value = malloc(sizeof(char) * 10);
		sprintf(value, "%d", 3 * i);
		printf("%s\n", value);
		Enter(table, str, value);
	}

	string s = Lookup(table, "100");
	printf("%s:%s\n", "100", s);
	
	return 0;
}
